import java.util.*;

Frame myFrame = null;
JSONObject json, curr_obj, curr_obj1;
JSONArray arr, arr1;
void setup() {
  size(800, 800);  
  selectInput("Select a file to process:", "fileSelected");
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
    selectInput("Select a file to process:", "fileSelected");
  } 
  else {
    println("User selected " + selection.getAbsolutePath());

    ArrayList<GraphVertex> verts = new ArrayList<GraphVertex>();
    ArrayList<GraphEdge>   edges = new ArrayList<GraphEdge>();
    Hashtable<String,GraphVertex> store = new Hashtable<String,GraphVertex>();
      
    // TODO: PUT CODE IN TO LOAD THE GRAPH    
     json = loadJSONObject(selection);
     arr  = json.getJSONArray("nodes");
     arr1 = json.getJSONArray("links");
     
    // arr= parseJSONArray("nodes");
    if(arr == null || arr1 == null){
      println("null");
    }
    else{
      
      //println(arr);
      for(int i=0; i<arr.size(); i++){
        curr_obj = arr.getJSONObject(i);
        
        String id = curr_obj.getString("id");
        
        int group = curr_obj.getInt("group");
       
        
        float x = (float) random(100,600);
        float y = random(100,600);
        verts.add(new GraphVertex(id,group,x,y));
        
        store.put(id,new GraphVertex(id,group,x,y));
      }
      
      for(int j=0; j<arr1.size();j++){
        curr_obj1 = arr1.getJSONObject(j);
        
        String src = curr_obj1.getString("source");
        String des = curr_obj1.getString("target");
        GraphVertex v1 = store.get(src);
        GraphVertex v2 =store.get(des); 
        
        Float w = curr_obj1.getFloat("value");
        
        edges.add(new GraphEdge(v1,v2,w));
        
        
        
      }
      
    }
    

    myFrame = new ForceDirectedLayout( verts, edges );
  }
}


void draw() {
  background( 255 );

  if ( myFrame != null ) {
    myFrame.setPosition( 0, 0, width, height );
    myFrame.draw();
  }
}

void mousePressed() {
  myFrame.mousePressed();
}

void mouseReleased() {
  myFrame.mouseReleased();
}

abstract class Frame {

  int u0, v0, w, h;
  int clickBuffer = 2;
  void setPosition( int u0, int v0, int w, int h ) {
    this.u0 = u0;
    this.v0 = v0;
    this.w = w;
    this.h = h;
  }

  abstract void draw();
  
  void mousePressed() { }
  void mouseReleased() { }

  boolean mouseInside() {
    return (u0-clickBuffer < mouseX) && (u0+w+clickBuffer)>mouseX && (v0-clickBuffer)< mouseY && (v0+h+clickBuffer)>mouseY;
  }
  
}