// most modification should occur in this file


class ForceDirectedLayout extends Frame {
  
  
  float RESTING_LENGTH = 10.0f;   // update this value
  float SPRING_SCALE   = 0.0075f; // update this value
  float REPULSE_SCALE  = 400.0f;  // update this value

  float TIME_STEP      = 0.5f;    // probably don't need to update this

  // Storage for the graph
  ArrayList<GraphVertex> verts;
  ArrayList<GraphEdge> edges;

  // Storage for the node selected using the mouse (you 
  // will need to set this variable and use it) 
  GraphVertex selected = null;
  

  ForceDirectedLayout( ArrayList<GraphVertex> _verts, ArrayList<GraphEdge> _edges ) {
    verts = _verts;
    edges = _edges;
  }

  void applyRepulsiveForce(  GraphVertex v0, GraphVertex v1 ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A REPULSIVE FORCE
    
  
  }

  void applySpringForce( GraphEdge edge ) {
    // TODO: PUT CODE IN HERE TO CALCULATE (AND APPLY) A SPRING FORCE
    
  
  }

  void draw() {
    update(); // don't modify this line
    
    // TODO: ADD CODE TO DRAW THE GRAPH
    background(255,255,157);
    for(int i = 0; i<arr.size(); i++){
      PVector pos =verts.get(i).getPosition();
      fill(255,0,0);
      rect(pos.x,pos.y,15,15);
      if(mouseX >= pos.x && mouseX <=pos.x+10 && mouseY >= pos.y && mouseY <=pos.y+10){
      
         String name = verts.get(i).getID(); 
         fill(128,0,0);
         textSize(15);
         text(name,mouseX,mouseY);
         textSize(10);
         fill(255);
      }
     
     for(int j=0;j<arr1.size();j++){
       GraphVertex v0 = edges.get(j).v0;
       GraphVertex v1 = edges.get(j).v1;
       
       PVector pos0 = v0.getPosition();
       PVector pos1 = v1.getPosition();
       
       line(pos0.x,pos0.y,pos1.x,pos1.y);
       
       
     }
    
    
    }
    
    
  }



  void mousePressed() { 
    // TODO: ADD SOME INTERACTION CODE
   
    

  }

  void mouseReleased() {    
    // TODO: ADD SOME INTERACTION CODE

  }



  // The following function applies forces to all of the nodes. 
  // This code does not need to be edited to complete this 
  // project (and I recommend against modifying it).
  void update() {
    for ( GraphVertex v : verts ) {
      v.clearForce();
    }

    for ( GraphVertex v0 : verts ) {
      for ( GraphVertex v1 : verts ) {
        if ( v0 != v1 ) applyRepulsiveForce( v0, v1 );
      }
    }

    for ( GraphEdge e : edges ) {
      applySpringForce( e );
    }

    for ( GraphVertex v : verts ) {
      v.updatePosition( TIME_STEP );
    }
  }
}