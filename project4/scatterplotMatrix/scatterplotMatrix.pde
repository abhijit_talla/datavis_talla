Table table;
int total, yAxis, satm, satv, act, selector;
float gpa;

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");

}

//getting the table as input
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

//keyboard functions
void keyPressed(){
  if (key == '1') {
     selector = 1;
  }if(key == '2'){
    selector=2;
  }if(key == '3'){
    selector=3;
  }if(key == '4'){
    selector=4;
  }if(key == '5'){
    selector=5;
  }if(key == '6'){
    selector=6;
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);
  //Enhancedvgraph background dimentions
  rect(450,yAxis-375,300,300);
  //helping commands
   fill (0);
  text( "Press #1 - SATM VS SATV", 50, yAxis-25);
  text( "Press #2 - SATM VS ACT", 50, yAxis);
  text( "Press #3 - SATM VS GPA", 50, yAxis+25);
  text( "Press #4 - SATV VS GPA", 250, yAxis-25);
  text( "Press #5 - SATV VS ACT", 250, yAxis);
  text( "Press #6 - ACT VS GPA", 250, yAxis+25);
  fill (255);
  
  //matrix plot dimentions
  rect(90,yAxis-375,100,100);
  rect(90,yAxis-275,100,100);
  rect(90,yAxis-175,100,100);
  rect(190,yAxis-175,100,100);
  rect(290,yAxis-175,100,100);
  rect(190,yAxis-275,100,100);
  fill(0);
  text("SATM",120, yAxis-380);
  text("SATV",45,yAxis-125);
  text("ACT",(300), yAxis-220);
  text("GPA",200,yAxis-320);
  
  
  while(i < total){
    //fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    satv = newrow.getInt("SATV");
    satm = newrow.getInt("SATM");
    act  = newrow.getInt("ACT");
    gpa  = newrow.getFloat("GPA");
    
    fill(255);
    //SATMvsSATV
    ellipse(50+satm/7,yAxis-50-(satv/7),.5,.5);
    //SATMvsACT
    ellipse(50+satm/7,yAxis-120-(act*4),.5,.5);
    //SATMvsGPA
    ellipse(50+satm/7,yAxis-300-(gpa*15),.5,.5);
    //SATVvsGPA
    ellipse(160+satv/7,yAxis-100-(gpa*15),.5,.5);
    //SATVvsACT
    ellipse(250+satm/7,yAxis-25-(act*4),.5,.5);
    //ACTvsGPA
    ellipse(140+act*4,yAxis-200-(gpa*15),.5,.5);
    
    i++; 
  fill(0);
  //now to print on the big screen
  
 if(selector == 1){
   text("SATM",575,yAxis-50);
   text("SATV",400,yAxis-225);
   ellipse(400+satm/3, yAxis-80-(satv/3),2,2);
  }
  if(selector == 2){
    text("SATM",575,yAxis-50);
    text("ACT",400,yAxis-225);
    ellipse(300+satm/2,yAxis-120-(act*5),2,2);
  }if(selector == 3){
    text("SATM",575,yAxis-50);
    text("GPA",400,yAxis-225);
    ellipse(300+satm/2,yAxis-(gpa*80),2,2);
  }if(selector == 4){
    text("SATV",575,yAxis-50);
    text("GPA",400,yAxis-225);
    ellipse(320+satv/2,yAxis-(gpa*80),2,2);
  }if(selector == 5){
    text("SATV",575,yAxis-50);
    text("ACT",400,yAxis-225);
    ellipse(320+satv/2,yAxis-(act*10),2,2);
  }if(selector == 6){
    text("ACT",575,yAxis-50);
    text("GPA",400,yAxis-225);
    ellipse(320+act*10,yAxis-(gpa*75),2,2);
  }   
  }
  
}




 