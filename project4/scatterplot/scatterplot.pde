Table table;
int total, yAxis, satm, satv, act, selector;
float gpa;

void setup(){
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected");
}


public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void keyPressed(){
  if (key == '1') {
     selector = 1;
  }if(key == '2'){
    selector=2;
  }if(key == '3'){
    selector=3;
  }if(key == '4'){
    selector=4;
  }if(key == '5'){
    selector=5;
  }if(key == '6'){
    selector=6;
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);

  //graph dimention
  rect(90, 25, 525, 500);
  fill (0);
  text( "Press #1 - SATM VS SATV", 620, 40);
  text( "Press #2 - SATM VS ACT", 620, 60);
  text( "Press #3 - SATM VS GPA",620, 80);
  text( "Press #4 - SATV VS ACT", 620, 100);
  text( "Press #5 - SATV VS GPA", 620, 120);
  text( "Press #6 - ACT VS GPA", 620, 140);
  
  
  while(i < total){
    fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    satv = newrow.getInt("SATV");
    satm = newrow.getInt("SATM");
    act  = newrow.getInt("ACT");
    gpa  = newrow.getFloat("GPA");
    
    
    //drawing the scatter plot
    if(selector == 1){
      //legend
      text("SATM", 350, yAxis+75);
      text("SATV", 10, yAxis-250);
      scale(1,2);
      rect(75+satm/2, yAxis-(satv/2), 2.5,2.5);
      
      if (mouseX > 75+satm/2 && mouseX < 77.5+satm/2 && 
      mouseY > yAxis-(satv/2) && mouseY < yAxis+2.5-(satv/2)){
       textSize(14);
       fill(0);
       rect(72+satm/2,yAxis+2-(satv/2),62,-16);
       fill(225,225,0);
       text(satm+","+satv,75+satm/2,yAxis-(satv/2));
       fill(153, 0, 18);
      }
      
  
    }else if(selector == 2){
      //legend 
      text("SATM", 350, yAxis+75);
      text("ACT", 10, yAxis-250);
      scale(1,3);
      rect(75+satm/2, yAxis-(act*10), 2.5, 2.5);
      
      if (mouseX > 75+satm/2 && mouseX < 77.5+satm/2 && 
      mouseY > yAxis-(act*10) && mouseY < yAxis+2.5-(act*10)){
       textSize(14);
       fill(0);
       rect(72+satm/2,yAxis+2-(act*10),62,-16);
       fill(225,225,0);
       text(satm+","+act,75+satm/2,yAxis-(act*10));
       fill(153, 0, 18);
      }
      
     
    }else if(selector == 3){
      //legend 
      text("SATM", 350, yAxis+75);
      text("GPA", 10, yAxis-245);
      scale(1,4);
      rect(75+satm/2,yAxis-(gpa*75), 2.5, 2.5);
      
      if (mouseX > 75+satm/2 && mouseX < 77.5+satm/2 && 
      mouseY > yAxis-(gpa*75) && mouseY < yAxis+2.5-(gpa*75)){
       textSize(14);
       fill(0);
       rect(72+satm/2,yAxis+2-(gpa*75),80,-16);
       fill(225,225,0);
       text(satm+","+gpa,75+satm/2,yAxis-(gpa*75));
       fill(153, 0, 18);
      }
      
  
    }else if(selector == 4){
      //legend 
      text("SATV", 350, yAxis+75);
      text("ACT", 10, yAxis-250);
      scale(2,3);
      rect(75+satv/2, yAxis-(act*10), 2.5, 2.5);
      
      if (mouseX > 75+satv/2 && mouseX < 77.5+satv/2 && 
      mouseY > yAxis-(act*10) && mouseY < yAxis+2.5-(act*10)){
       textSize(14);
       fill(0);
       rect(72+satv/2,yAxis+2-(act*10),62,-16);
       fill(225,225,0);
       text(satv+","+act,75+satv/2,yAxis-(act*10));
       fill(153, 0, 18);
      }
      
      
    }else if(selector == 5){
      //legend 
      text("SATV", 350, yAxis+75);
      text("GPA", 10, yAxis-245);
      scale(2,4);
      rect(75+satv/2,yAxis-(gpa*75), 2.5, 2.5);
      
      if (mouseX > 75+satv/2 && mouseX < 77.5+satv/2 && 
      mouseY > yAxis-(gpa*75) && mouseY < yAxis+2.5-(gpa*75)){
       textSize(14);
       fill(0);
       rect(72+satv/2,yAxis+2-(gpa*75),80,-16);
       fill(225,225,0);
       text(satv+","+gpa,75+satv/2,yAxis-(gpa*75));
       fill(153, 0, 18);
      }
      
      
      
    }else if(selector == 6){
      //legend 
      text("ACT", 350, yAxis+75);
      text("GPA", 10, yAxis-245);
      scale(3,4);
      rect(110+act*10, yAxis-(gpa*75), 2, 2);
      
      if (mouseX > 110+act*10 && mouseX < 115+act*10 && 
      mouseY > yAxis-(gpa*75) && mouseY < yAxis+5-(gpa*75)){
       textSize(14);
       fill(0);
       rect(72+act*10,yAxis+2-(gpa*75),75,-16);
       fill(225,225,0);
       text(act+","+gpa,75+act*10,yAxis-(gpa*75));
       fill(153, 0, 18);
      }
      
      
    }else{
      textSize(35);
      text("PLEASE MAKE A SELECTION",120,yAxis-200);
      textSize(14);
    }
    i++;
  }

   
}
//marking out scales
void scale(int x, int y){
   if( x==1 ){
    int j= 0;
    while(j<=900){
      fill(0, 0, 160);
      text(j, 100+(j/2.20), yAxis+50);
      j = j+100;
     }
   }
   if(x == 3){   
      // setup the ACT scale
     int j= 0;
     while(j<=40){
       fill(0, 0, 160);
       text(j, 100+(j*10), yAxis+50);
       j = j+5;
     }
   }
   if(y ==3){
      int j= 0;
      while(j<=40){
        fill(0, 0, 160);
        text(j, 45, yAxis-(j*10));
        j = j+5;
      }
    }  
    if( y==4 ){
      float k=0;
      while(k<=4){
        fill(0, 0, 160);
        text(k, 25, yAxis-k*75);
        k=k+0.5;  
      }
    }
    if(x==2 ){
      int j= 0;
      while(j<=900){
        fill(0, 0, 160);
        text(j, 100+(j/2.20), yAxis+50);
        j = j+100;
      } 
    }
    if(y==2){
      int j = 0;
      while(j<=900){
        fill(0, 0, 160);
        text(j, 55, (yAxis-(j/2)));
        j = j+100;
      }  
    }  
  
}