Table table;
int total, yAxis, satm, satv, act, selector;

int satm1, satv1, act1;
float gpa, gpa1;

void setup(){
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected");
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void keyPressed(){
  if (key == '1') {
     selector = 1;
  }if(key == '2'){
    selector=2;
  }if(key == '3'){
    selector=3;
  }if(key == '4'){
    selector=4;
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);
  //fill(0);
  //graph dimention
  rect(85, 25, 550, 500);
  fill (0);
  text( "Press #1 - SATM ", 660, 40);
  text( "Press #2 - SATV ", 660, 60);
  text( "Press #3 - ACT ",660, 80);
  text( "Press #4 - GPA ", 660, 100);
  
  //plotting bar charts
  while(i < total-1){
    fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    TableRow nextrow = table.getRow(i+1);
    satv = newrow.getInt("SATV");
    satm = newrow.getInt("SATM");
    act  = newrow.getInt("ACT");
    gpa  = newrow.getFloat("GPA");
    
    satv1 =nextrow.getInt("SATV");
    satm1 =nextrow.getInt("SATM");
    act1 =nextrow.getInt("ACT");
    gpa1 =nextrow.getFloat("GPA");
    
    //drawing the SATM plot
    if(selector == 1){  
      //legend
      text("SATM", 350, yAxis+75);
      scale(1);
     line(90+i*2, yAxis-satm/2,90+(i+1)*2,yAxis-(satm1/2));
     //plotting points
     fill(255,0,0);
     ellipse(90+i*2,yAxis-satm/2,5,5);
     
      //adding interaction to chart
      if (mouseX >= 90+i*2 &&  mouseX <= 95+(i)*2 &&
      mouseY >= yAxis-satm/2 && mouseY <= yAxis+5-(satm/2)){
        textSize(14);
        fill(0);
        rect(295,yAxis-395,35,-20);
        fill(225,225,0);
        text(satm,300,yAxis-400);
        fill(153, 0, 18);
      }
       
    //drawing the SATV plot
    }else if(selector == 2){
      //legend 
      text("SATV", 350, yAxis+75);
      scale(2);
      line(90+i*2, yAxis-satv/2,90+(i+1)*2,yAxis-(satv1/2));
      //plotting points
      fill(255,0,0);
      ellipse(90+i*2,yAxis-satv/2,5,5);
      //adding interaction to chart
      if (mouseX >= 90+i*2 &&  mouseX <= 95+i*2 &&
      mouseY <= yAxis+5-(satv/2) && mouseY >= yAxis-(satv/2)){
        textSize(14);
        fill(0);
        rect(295,yAxis-395,35,-20);
        fill(225,225,0);
        text(satv,300,yAxis-400);
        fill(153, 0, 18);
      
      }
    //drawing the ACT plot
    }else if(selector == 3){
       //legend 
      text("ACT", 350, yAxis+75);
      scale(3);
      fill(0,0,255);
     
      line(90+i*2, yAxis-act*10,90+(i+1)*2,yAxis-(act1*10));
      //plotting points
      fill(255,0,0);
      ellipse(90+i*2,yAxis-act*10,5,5);
      //adding interaction to chart
      if (mouseX >= 90+i*2 &&  mouseX <= 95+i*2 &&
      mouseY <= yAxis+5-(act*10) && mouseY >= yAxis-(act*10)){
       textSize(14);
       fill(0);
       rect(295,yAxis-395,35,-20);
       fill(225,225,0);
       text(act,300,yAxis-400);
       fill(153, 0, 18);
      }
      
    //drawing the GPA plot
    }else if(selector == 4){
      text("GPA", 350, yAxis+75);
      scale(4);
      fill(0,0,255);
      line(90+i*2, yAxis-gpa*75,90+(i+1)*2,yAxis-(gpa1*75));
      //plotting points
      fill(255,0,0);
      ellipse(90+i*2,yAxis-gpa*75,5,5);
      //adding interaction to chart
      if (mouseX >= 90+i*2 &&  mouseX <= 95+i*2 &&
      mouseY <= yAxis+5-(gpa*75) && mouseY >= yAxis-(gpa*75)){
       fill(0);
       rect(295,yAxis-395,55,-20);
       fill(225,225,0);
       text(gpa,300,yAxis-400);
       fill(153, 0, 18);
      }
     }else{
      textSize(35);
      text("PLEASE MAKE A SELECTION",120,yAxis-200);
      textSize(14);
    }
    i++;
  }

   
}
//marking out scales
void scale(int x){
  //SATM scale
   if( x==1 ){
    int j= 0;
    while(j<=900){
      fill(0, 0, 160);
      text(j,55, yAxis+25-(j/2.0));
      j = j+100;
     }
   }
   //SATV scale
   if(x==2){
      int j = 0;
      while(j<=900){
        fill(0, 0, 160);
        text(j, 55, (yAxis+25-(j/2)));
        j = j+100;
      } 
   }
   //ACT scale
   if(x ==3){
      int j= 0;
      while(j<=40){
        fill(0, 0, 160);
        text(j, 60, yAxis+25-(j*10));
        j = j+5;
      }
    }  
    //GPA scale
    if( x==4 ){
      float k=0;
      while(k<=4){
        fill(0, 0, 160);
        text(k, 40, yAxis+25-k*75);
        k=k+0.5;  
      }
    }  
  
}