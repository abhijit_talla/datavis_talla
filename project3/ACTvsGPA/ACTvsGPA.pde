Table table;
int total, yAxis, act;
float gpa;
void setup(){
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected");

}
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);
  //background for the chart
  line(90, 75, 550, 75);
  line(90, 75, 90, 525);
  line(90, 525, 550, 525);
  line(550, 75, 550, 525);
  // the chart size
  rect(90, 75, 525, 450);
  fill (0);
  //mentioning the Legend
  text("ACT", (300+(52*i)), yAxis+75);
  text("GPA", 30, yAxis-325);
  
  while(i < total){
    fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    act = newrow.getInt("ACT");
    gpa = newrow.getFloat("GPA");
    //marking the points
    ellipse(110+act*10, yAxis-(gpa*75), 2, 2);
    i++;  
  }
  // setup the ACT scale
  int j= 0;
  while(j<=40){
    fill(0, 0, 160);
    text(j, 100+(j*10), yAxis+50);
    j = j+5;
  }
  //setup GPA scale
  float k=0;
  while(k<=4){
    fill(0, 0, 160);
    text(k, 25, yAxis-k*75);
    k=k+0.5;
  }
  
}