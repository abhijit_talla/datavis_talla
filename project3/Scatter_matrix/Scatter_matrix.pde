Table table;
int total, yAxis, satm, satv, act;
float gpa;

void setup(){
  size(800,600);
  selectInput("Select a file to process:", "fileSelected");

}

public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);
  //drawing matrix plots
  rect(90, yAxis-460, 180, 180);
  rect(90, yAxis-280, 180, 180);
  rect(90, yAxis-100, 180, 180);
  rect(270, yAxis-280, 180, 180);
  rect(450, yAxis-100, 180, 180);
  rect(270, yAxis-100, 180, 180);
  fill(0);
  //marking Legends
  text("SATM", 160, yAxis-470);
  text("SATV", 640, yAxis);
  text("ACT", 460, yAxis-180);
  text("GPA", 280, yAxis-370);
  
  
  while(i < total){
    //fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    satv = newrow.getInt("SATV");
    satm = newrow.getInt("SATM");
    act  = newrow.getInt("ACT");
    gpa  = newrow.getFloat("GPA");
    
    fill(255);
    //SATMvsSATV
    ellipse(50+satm/4, yAxis+120-(satv/4), 1, 1);
    //SATMvsACT
    ellipse(50+satm/4, yAxis-50-(act*6), 1, 1);
    //SATMvsGPA
    ellipse(50+satm/4, yAxis-300-(gpa*30), 1, 1);
    //GPAvsSATV
    ellipse(220+(gpa*45), yAxis+120-satv/4, 1, 1);
    //ACTvsSATV
    ellipse(400+(act*6), yAxis+125-(satv/4), 1, 1);
    //GPAvsACT
    ellipse(250+(gpa*40), yAxis-50-(act*6), 1, 1);
    
    i++; 
    textSize(12);
  }
  //scale for SATM & SATV
  int j= 0;
  while(j<=900){
    fill(0, 0, 160);
    text(j, 90+j/5.9, yAxis+95);
    text(j, 65, yAxis+80-j/5.6);
    j = j+450;
  }
  //Scale for ACT
  j=0;
  while(j<=40){
    fill(0, 0, 160);
    text(j, 65, yAxis-100-j*4);
    text(j, 450+j*4, yAxis+95);
    j=j+20;
  }
  //Scale for GPA
  float k =0;
  while(k <= 4.0){
    fill(0, 0, 160);
    text(k,40, yAxis-280-(k*42));
    text(k,268+(k*34.5), yAxis+95);
    k=k+2.0;
  }
  //emblishements 
  line(90, yAxis-280, 25, yAxis-280);
  line(90, yAxis-100, 25, yAxis-100);
  line(270, yAxis+50, 270, yAxis+100);
  line(450, yAxis+50, 450, yAxis+100);
  
  textSize(14);
  //legend
  text("The graphs were plotted as X vs Y axis:", 500,yAxis-470);
  text("SATM vs GPA  at matrix(1x1)", 520, yAxis-450);
  text("SATM vs ACT  at matrix(2x1)", 520, yAxis-430);
  text("SATM vs SATV at matrix(3x1)", 520, yAxis-410);
  text("GPA vs ACT   at matrix(2x2)", 520, yAxis-390);
  text("GPA vs SATV  at matrix(3x2)", 520, yAxis-370);
  text("ACT vs SATV  at matrix(3x3)", 520, yAxis-350);
}




 