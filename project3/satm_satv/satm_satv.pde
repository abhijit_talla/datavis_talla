Table table;
int total, yAxis, satm, satv;
void setup(){
  size(800, 600);
  selectInput("Select a file to process:", "fileSelected");

}
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(229, 228, 226);
  textSize(14);
  int i = 0;
  yAxis = 500;
  fill(235, 244, 250);
  //border for the window
  line(90, 25, 550, 25);
  line(90, 25, 90, 525);
  line(90, 525, 550, 525);
  line(550, 25, 550, 525);
  //graph dimention
  rect(90, 25, 525, 500);
  fill (0);
  //legend
  text("SATM", (300+(52*i)), yAxis+75);
  text("SATV", 10, yAxis-250);
  
  while(i < total){
    fill(153, 0, 18);
    TableRow newrow = table.getRow(i);
    satv = newrow.getInt("SATV");
    satm = newrow.getInt("SATM");
    //drawing the scatter plot
    ellipse(75+satm/2, yAxis-(satv/2), 2.5, 2.5);
    i++;
  }
  //marking out scales
  int j= 0;
  while(j<=900){
    fill(0, 0, 160);
    text(j, 100+(j/2.20), yAxis+50);
    text(j, 55, (yAxis-(j/2)));
    j = j+100;
  }  
}