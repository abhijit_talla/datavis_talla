

void scatterMatrix(){
  int idx = 0;
  fill(255);
  //setting up initial matrices
  rect(300,height-350,75,75);
  rect(300,height-275,75,75);
  rect(300,height-200,75,75);
  rect(225,height-350,75,75);
  rect(150,height-350,75,75);
  rect(225,height-275,75,75);
  rect(50,height-200,175,175);
  //adjusting data
  for(int i=0;i<total;i++){
    one[i]=five[i]*2.5;
    two[i]=six[i]*2.5;
    three[i]=seven[i]/8.5;
    four[i]=eight[i]/80;
  }
  for(int i=0;i<total;i++){
    one[i]=one[i]/5;
    two[i]=two[i]/5;
    three[i]=three[i]*5;
    four[i]=four[i]*40;
  }
    
  
  while(idx < total-1){
    //initial plot
    //satm vs satv
    ellipse(150+var1[idx]/12,height-250-(var2[idx]/10),.5,.5);
    //satm vs act
    ellipse(225+var1[idx]/12,height-250-(var3[idx]*2.5),.5,.5);
    //satm vs gpa
    ellipse(300+var1[idx]/12,height-250-(var4[idx]*15),.5,.5);
    //satv vs act
    ellipse(225+var2[idx]/12,height-200-(var4[idx]*15),.5,.5);
    //satv vs gpa
    ellipse(300+var2[idx]/12,height-200-(var3[idx]*2),.5,.5);
    //act vs gpa
    ellipse(300+var3[idx]*2,height-100-(var4[idx]*20),.5,.5);
    //drawing for satm vs satv
    if(selector3 ==1){
      fill(0,0,255);
      text(table.getColumnTitle(0),10,height-75);
      text(table.getColumnTitle(1),100,height-5);
      g=one;h=two;
      fill(255);
    }
    //drawing for satm vs act
    else if(selector3 ==2){
      fill(0,0,255);
      text(table.getColumnTitle(0),10,height-75);
      text(table.getColumnTitle(2),100,height-5);
       g=one;h=three;
       fill(255);
    }
    //drawing satm vs gpa
    else if(selector3 == 3){
      fill(0,0,255);
      text(table.getColumnTitle(0),10,height-75);
      text(table.getColumnTitle(3),100,height-5);
      g=one;h=four;
      fill(255);
    }
    //drawing satv vs act
    else if(selector3 == 4){
      fill(0,0,255);
      text(table.getColumnTitle(1),10,height-75);
      text(table.getColumnTitle(2),100,height-5);
      g=two;h=three;
      fill(255);
    }
    //drawing satv vs gpa
    else if(selector3 == 5){
      fill(0,0,255);
      text(table.getColumnTitle(1),10,height-75);
      text(table.getColumnTitle(3),100,height-5);
      g=two;h=four;
      fill(255);
    }
    //drawing act vs gpa
    else if(selector3 == 6){
      fill(0,0,255);
      text(table.getColumnTitle(2),10,height-75);
      text(table.getColumnTitle(3),100,height-5);
      g=three;h=four;
      fill(255);
    }
    else{
      fill(0,0,255);
      text(table.getColumnTitle(0),10,height-75);
      text(table.getColumnTitle(1),100,height-5);
      g=one;h=two;
      fill(255);
    }
    stroke(0);
    fill(255,0,0);
    //plotting the points
    ellipse(50+g[idx], height-(h[idx]),2,2);
    stroke(0);
    fill(255);
    //checking mouse location
    if(mouseX > 50+g[idx] && mouseX<52+g[idx] && mouseY >=height-h[idx] && mouseY<=height-h[idx]+2){
       fill(255,0,0);
      //bar
  text((int)val[idx],10,380-e[idx]);
  line(40,380-e[idx],40.5+idx*1.2,380-e[idx]);
  //line
  text((int)val[idx],410,380-e[idx+1]);
  line(440,380-e[idx+1],440+idx*1.2,380-e[idx+1]);
  //scatter
  text((int)val[idx],810,380-f[idx]);
  text((int)val1[idx],850+e[idx],390);
  line(840,380-f[idx],850+e[idx],380-f[idx]);
  line(850+e[idx],350,850+e[idx],382.5-f[idx]);
  //scatterMatrix
  line(50,height-h[idx],50+g[idx],height-h[idx]);
  line(50+g[idx],height-25,50+g[idx],height-h[idx]+2);
  //parallelplot
  stroke(255);
  line(450,height-a[idx],683,height-b[idx]);
  line(683,height-b[idx],916,height-c[idx]);
  line(916,height-c[idx],1150,height-d[idx]);
  stroke(0);  
  fill(255);
      
    }
  

idx++;
}
  
  
  
}