
void linechart(){
  int idx =0;
  fill(229, 228, 226);
  rect(560,5,80,20);
  fill(0);
  text("SWITCH",580,20);
  fill(255);
  line(440,50,440,380);
  line(440,380,780,380); 
  while( idx<total-1){
    //satm
    if(selector ==0){
      e =five;
      val =five;
      for(int d= 0; d<=max1 ;d+=max1/4){
        fill(0,0,255);
        text(table.getColumnTitle(0),420,20);
        text(d,410,380-d/2.5);
        fill(255);
      }
    }
    //satv
    if(selector == 1){
      e=six;
      val = six;
      for(int d= 0; d<=max2 ;d+=max2/4){
        fill(0,0,255);
        text(table.getColumnTitle(1),420,20);
        text(d,410,380-d/2.5);
        fill(255);
      }
    }
    //act
    if(selector ==2){
      e=seven;
      val=seven;
      for(int d= 0; d<=max3 ;d+=max3/4){
        fill(0,0,255);
        text(table.getColumnTitle(2),420,20);
        text(d,410,380-d*9);
        fill(255);  
    }
    }
    //gpa
    if(selector == 3){
      e=eight;
      val =eight;
      for(int d= 0; d<=max4 ;d+=max4/4){
        fill(0,0,255);
        text(table.getColumnTitle(3),420,20);
        text(d,410,380-d*80);
        fill(255);
      }
      
    }
    //drawing the line chart
    line(440+(idx*1.2),380-e[idx],440+((idx*1.2)+1),380-e[idx+1]);
    //mouse location
     if( mouseX >= 440+idx*1.2 && mouseX <= 440.5+idx*1.2 && mouseY <= 380-e[idx] && mouseY >= 380-e[idx+1]){
        fill(255,0,0);  
       //bar
  text((int)val[idx],10,380-e[idx]);
  line(40,380-e[idx],40.5+idx*1.2,380-e[idx]);
  //line
  text((int)val[idx],410,380-e[idx+1]);
  line(440,380-e[idx+1],440+idx*1.2,380-e[idx+1]);
  //scatter
  text((int)val[idx],810,380-f[idx]);
  text((int)val1[idx],850+e[idx],390);
  line(840,380-f[idx],850+e[idx],380-f[idx]);
  line(850+e[idx],350,850+e[idx],382.5-f[idx]);
  //scatterMatrix
  line(50,height-h[idx],50+g[idx],height-h[idx]);
  line(50+g[idx],height-25,50+g[idx],height-h[idx]+2);
  //parallelplot
  stroke(255);
  line(450,height-a[idx],683,height-b[idx]);
  line(683,height-b[idx],916,height-c[idx]);
  line(916,height-c[idx],1150,height-d[idx]);
  stroke(0);
  fill(255);
        
      }
    
  idx++;
  }
 
}