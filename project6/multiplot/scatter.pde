void scatter(){
  int idx =0;
  fill(229, 228, 226);
  rect(960,5,80,20);
  fill(0);
  text("SWITCH",980,20);
  fill(255);
  
  line(840,50,840,350);
  line(840,350,1180,350);
 
  while( idx<total-1){
    //getting satm vs satv
    if(selector2 == 0){
      e=five;
      f=six;
      val1 = var2; val= var1;
      for(int d= 0; d<=max1 ;d+=max1/4){
        fill(0,0,255);
        text(table.getColumnTitle(0),820,20);
        text(table.getColumnTitle(1),820,390);
        text(d,810,380-d/2.5);
        text(d,810+d/2.25,380);
        fill(255);
      } 
    }
    //getting satm vs act
    if(selector2 == 1){
      e=five;
      f=seven;
      val = var1; val1= var3;
      for(int d= 0; d<=max1 ;d+=max1/4){
        fill(0,0,255);
        text(table.getColumnTitle(0),820,20);
        text(table.getColumnTitle(2),820,390);
        text(d,810,380-d/2.5);
        fill(255);
      }
      for(int d= 0; d<=max3 ;d+=max3/4){
        fill(0,0,255);
        text(d,810+d*8.5,380);
        fill(255);
      }
    }
    //getting satm vs gpa
    if(selector2 ==2){
      e=five;
      f=eight;
      val = var1; val1= var4;
      for(int d= 0; d<=max1 ;d+=max1/4){
        fill(0,0,255);
        text(table.getColumnTitle(0),820,20);
        text(table.getColumnTitle(3),820,390);
        text(d,810,380-d/2.5);
        fill(255);
      }
      for(int d= 0; d<=max4 ;d+=max4/4){
        fill(0,0,255);
        text(d,810+d*80,380);
        fill(255);
      }
    }
    //getting satv vs act
    if(selector2 == 3){
      e=six;
      f=seven;
      val =var2; val1= var3;
      for(int d= 0; d<=max2 ;d+=max2/4){
        fill(0,0,255);
        text(table.getColumnTitle(1),820,20);
        text(table.getColumnTitle(2),820,390);
        text(d,810,380-d/2.5);
        fill(255);
      }
      for(int d= 0; d<=max3 ;d+=max3/4){
        fill(0,0,255);
        text(d,810+d*8.5,380);
        fill(255);
      }
    }
    //getting satv vs gpa
    if(selector2 == 4){
      e=six;
      f=eight;
      val = var2; val1= var4;
      for(int d= 0; d<=max2 ;d+=max2/4){
        fill(0,0,255);
        text(table.getColumnTitle(1),820,20);
        text(table.getColumnTitle(3),820,390);
        text(d,810,380-d/2.5);
        fill(255);
      }
      for(int d= 0; d<=max4 ;d+=max4/4){
        fill(0,0,255);
        text(d,810+d*85,380);
        fill(255);
      }
    }
    //getting act vs gpa
    if(selector2 == 5){
      e=seven;
      f=eight;
      val1 = var4; val= var3;
      for(int d= 0; d<=max3 ;d+=max3/4){
        fill(0,0,255);
        text(table.getColumnTitle(2),820,20);
        text(table.getColumnTitle(3),820,390);
        text(d,810,380-d*8.5);
        fill(255);
      }
      for(int d= 0; d<=max4 ;d+=max4/4){
        fill(0,0,255);
        text(d,810+d*85,380);
        fill(255);
      }
    }
    rect(850+e[idx], 380-f[idx], 2.5,2.5);
    //mouse location 
    if (mouseX > 850+e[idx] && mouseX <852.5+e[idx] && 
      mouseY > 380-f[idx] && mouseY < 382.5-f[idx]){
         fill(255,0,0);
                 //bar
  text((int)val[idx],10,380-e[idx]);
  line(40,380-e[idx],40.5+idx*1.2,380-e[idx]);
  //line
  text((int)val[idx],410,380-e[idx+1]);
  line(440,380-e[idx+1],440+idx*1.2,380-e[idx+1]);
  //scatter
  text((int)val[idx],810,380-f[idx]);
  text((int)val1[idx],850+e[idx],390);
  line(840,380-f[idx],850+e[idx],380-f[idx]);
  line(850+e[idx],350,850+e[idx],382.5-f[idx]);
  //scatterMatrix
  line(50,height-h[idx],50+g[idx],height-h[idx]);
  line(50+g[idx],height-25,50+g[idx],height-h[idx]+2);
  //parallelplot
  stroke(255);
  line(450,height-a[idx],683,height-b[idx]);
  line(683,height-b[idx],916,height-c[idx]);
  line(916,height-c[idx],1150,height-d[idx]);
  stroke(0);
  fill(255);
      }
     
    
  idx++;
  }
}