Table table;
int year, year2, val1,val2;
int total;

void setup() {
  size(600,600);
  selectInput("Select a file to process:", "fileSelected");
  
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(120,0,0);
  textSize(14);
  int i = 0;
  //int total = table.getRowCount();
  int yAxis = 450;
  text("YEAR",(275+(52*i)), yAxis+40);
  text("VALUE1", 35, yAxis-125);
  //top
  line(90,150,500,150);
  //left
  line(90,150,90,470);
  //bottom
  line(90,470,500,470); 
  //right
  line(500,150,500,470);
  while (i < total-1 ){  
    TableRow newrow = table.getRow(i);
    TableRow nextrow = table.getRow(i+1);
    year = newrow.getInt("YEAR");
    year2 = nextrow.getInt("YEAR");
    val1 = newrow.getInt("VALUE1");
    val2 = nextrow.getInt("VALUE1");
    stroke(250);
    line((110+(52*i)), yAxis-val1*3,(110+(52*(i+1))), yAxis-val2*3);
    fill(255, 255,0);
    text(year,(100+(52*i)), yAxis+12);
    text(val1,(125+(52*i)), yAxis-val1*3);
    i++;   
  }
  //details of last point
  text(year2,(100+(52*i)), yAxis+12);
  text(val2,(125+(52*i)), yAxis-val2*3);
    
}