Table table;
int year, val1, total;
void setup() {
  size(600,600);
  selectInput("Select a file to process:", "fileSelected"); 
}
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(0);
  textSize(14);
  int i = 0;
  //int total = table.getRowCount();
  //println(total);
  int yAxis = 450;
  text("YEAR",(275+(52*i)), yAxis+40);
  text("VALUE1",35,yAxis-125);
  stroke(200);
  line(90,150,500,150);
  line(90,150,90,470);
  line(90,470,500,470);
  line(500,150,500,470);
  while (i < total){
    stroke(0);
    TableRow newrow = table.getRow(i);
    year = newrow.getInt("YEAR");
    val1 = newrow.getInt("VALUE1");
    //println(year,val1);
    fill(204, 102, 0);
    rect((110+(52*i)),yAxis, 50, -(val1*3));
    fill(240,255,0);
    text(year,(120+(52*i)), yAxis+12);
    text(val1,(125+(52*i)), yAxis-val1*3);
    //fill(0);
    //stroke(1);
    i++;
  }
    
}