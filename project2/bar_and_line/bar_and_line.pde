Table table;
int year, year2, val1, val2;
int total;
void setup() {
  size(600,600);
  selectInput("Select a file to process:", "fileSelected");
  
}
public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable("inputdata.csv","header,csv");
    total = table.getRowCount();
  }
}

void draw(){
  background(200);
  textSize(14);
  int i = 0;
  //println(total);
  int yAxis = 450;
  text("YEAR",(275+(52*i)), yAxis+40);
  text("VALUE1",35,yAxis-125);
  stroke(200);
  fill(0);
 
  while (i < total-1){
    stroke(0);
    TableRow newrow = table.getRow(i);
    TableRow nextrow = table.getRow(i+1);
    year = newrow.getInt("YEAR");
    val1 = newrow.getInt("VALUE1");
    val2 = nextrow.getInt("VALUE1");
    year2 = nextrow.getInt("YEAR");
    //println(year,val1);
    fill(204, 102, 0);
    stroke(0);
    rect((110+(52*i)),yAxis, 50, -(val1*3));
  
    line((135+(52*i)), yAxis-val1*3,(135+(52*(i+1))), yAxis-val2*3);
    //rect((110+(52*i)),yAxis, 50, -(val1*3));
    fill(0);
    text(year,(120+(52*i)), yAxis+12);
    text(val1,(125+(52*i)), yAxis-val1*3);

    i++;
  }
  fill(204, 102, 0);
  rect((110+(52*i)),yAxis, 50, -(val2*3));
  fill(0);
  text(year2,(120+(52*i)), yAxis+12);
  text(val2,(125+(52*i)), yAxis-val2*3);  
}