void corrgram(){
 
  fill(0);
  text("SATM",855,height-720);
  text("SATV",930,height-645);
  text("ACT",1005,height-570);
  text("GPA",1080,height-495);
  text("PEARSON",1000,40);
  text("SPEARMAN",880,375);
  
  fill(0);
  float r12= pearson(var1,var2);
  fill(0,0,r12*255);
  rect(900,height-750,75,75);
  fill(255);
  text(r12,920,height-710);
  
  float r13= pearson(var1,var3);
  fill(0,0,r13*255);
  rect(975,height-750,75,75);
  fill(255);
  text(r13,995,height-710);
  
  
  float r14= pearson(var1,var4);
  fill(0,0,r14*255);
  rect(1050,height-750,75,75);
  fill(255);
  text(r14,1070,height-710);
  
  float r23= pearson(var2,var3);
  fill(0,0,r23*255);
  rect(975,height-675,75,75);
  fill(255);
  text(r23,995,height-630);
  
  float r24= pearson(var2,var4);
  fill(0,0,r24*255);
  rect(1050,height-675,75,75);
  fill(255);
  text(r24,1070,height-630);
  
  float r34= pearson(var3,var4);
   fill(0,0,r34*255);
  rect(1050,height-600,75,75);
  fill(255);
  text(r34,1070,height-560);
  //SPEARMAN
  float r21= spearman(var1,var2);
    fill(0,0,r21*255);
  rect(825,height-675,75,75);
  fill(255);
  text(r21,855,height-635);
  
  float r31 = spearman  (var1,var3);
  fill(0,0,r31*255);
  rect(825,height-600,75,75);
  fill(255);
  text(r31,855,height-550);
  
  float r32 = spearman(var2,var3);
  fill(0,0,r32*255);
  rect(900,height-600,75,75);
  fill(255);
  text(r32,925,height-550);
  
  float r41 = spearman(var1,var4);
    fill(0,0,r41*255);
  rect(825,height-525,75,75);
  fill(255);
  text(r41,855,height-475);
  
  float r42 = spearman(var2,var4);
  fill(0,0,r42*255);
  rect(900,height-525,75,75);
  fill(255);
  text(r42,925,height-475);
   
  
  float r43 = spearman(var3,var4);
  fill(0,0,r43*255);
  rect(975,height-525,75,75);
  fill(255);
  text(r43,1005,height-475);
  
  
}