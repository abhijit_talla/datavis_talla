float m=0,s=0,cov=0;
//to calculate pearson
float pearson(float[] a, float[] b){
  float cov = covariance(a,b);
  float std_a = std_dev(a);
  float std_b = std_dev(b);
  float pval = cov/(std_a*std_b);
  return pval;

}
//to calculate spearman
float spearman( float[] a, float[] b){
  float[] nb = rank(b);
  float[] na = rank(a);
  float sval = pearson(na,nb);
  return sval; 
}
//finding mean
float mean(float[] a){
  m=0;
  for(int i=0;i<total-2; i++){
      m = a[i]+m;
  }
  return m/total;
 
  
}
//finding standard deviation from mean
float std_dev( float[] a){
  s=0;
  float m =  mean(a);
  for(int i=0;i<total-2; i++){
      s = s + pow(a[i]-m,2);
  }
  return sqrt(s/total);
}
//finding covariance by using the mathematical formula
float covariance(float[] a, float[] b){
  cov=0;
  
  float x=mean(a); 
  float y=mean(b);
  for(int i=0;i<total-1; i++){
      cov = cov + ((a[i]-x)*(b[i]-y));
  }
  return cov/total;
}
 //rank calculator
float[] rank(float[] na){
  int rank;
  float[] rk;
  int j=0,i=0;
  rk = new float[total];
  while(i<total){
    rank=1;
    j=0;
    while(j<=total-1){
       if(na[j]>na[i])
          rank++;
       j++;
    } 
    rk[i] = rank;
    i++;
  }
  return rk;
}