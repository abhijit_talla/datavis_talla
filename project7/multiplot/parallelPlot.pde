void scale(float tmax1, float tmax2, float tmax3, float tmax4,float w,float x, float y, float z,int col1,int col2,int col3,int col4){
  fill(0,0,255);
  for(int n =0 ; n < tmax1 ; n+= tmax1/4){
    text(n,420,height-n*w);
    text(table.getColumnTitle(col1),430,440);
  }
  for(int n =0 ; n< tmax2 ; n+= tmax2/4){
    text(n,653,height-n*x);
    text(table.getColumnTitle(col2),663,440);
  }
  for(int n =0 ; n<tmax3 ; n+= tmax3/4){
    text(n,886,height-n*y);
    text(table.getColumnTitle(col3),896,440);
  }
  for(int n =0 ; n< tmax4 ; n+= tmax4/4){
    text(table.getColumnTitle(col4),1110,440);
    text(n,1120,height-n*z);
  }
  text((int)tmax1,420,height-tmax1*w);
  text((int)tmax2,653,height-tmax2*x);
  text((int)tmax3,886,height-tmax3*y);
  text((int)tmax4,1120,height-tmax4*z);
  fill(255);
  
} 



void parallelPlot(){

  a= five;
  b =six;
  c =seven;
  d= eight;
  int idx=0;
  strokeWeight(2);
  line(450,height-350,450,height-20);
  line(1150,height-350,1150,height-20);
  line(683,height-350,683,height-20);
  line(916,height-350,916,height-20);
  strokeWeight(1);
  fill(229, 228, 226);
  rect(410,405,80,20);
  rect(1100,405,80,20);
  rect(653,405,80,20);
  rect(886,405,80,20);
  fill(0);
  text("SWITCH",430,420);
  text("SWITCH",1120,420);
  text("SWITCH",906,420);
  text("SWITCH",673,420);
  fill(255);
   
  while(idx < total-2){
  //first axes
  if(selector4 ==1){
    if(count ==1){a= five; tmax1= max1; w =1/2.5;col1=0;}
    else if(count ==2){a=six; tmax1= max2; w= 1/2.5;col1=1;}
    else if(count ==3){ a = seven; tmax1= max3; w = 7.5;col1=2;}
    else if(count == 4){a = eight; tmax1= max4; w =80;col1=3;}
    scale(tmax1,tmax2,tmax3,tmax4,w,x,y,z,col1,col2,col3,col4); 

  }
  //2nd axes
  else if(selector4 ==2){
    if(count ==1){b= five;tmax2= max1;x =1/2.5;col2=0;}
    else if(count ==2){b=six;tmax2= max2;x =1/2.5;col2=1; }
    else if(count ==3){ b = seven;tmax2= max3;x = 6.5;col2=2;}
    else if(count == 4){b = eight;tmax2= max4;x =80;col2=3;}
    scale(tmax1,tmax2,tmax3,tmax4,w,x,y,z,col1,col2,col3,col4); 

  }
  //3rd axes
  else if(selector4 ==3){
    if(count ==1){c= five;tmax3= max1;y =1/2.5;col3=0;}
    else if(count ==2){c=six;tmax3= max2;y =1/2.5;col3=1;}
    else if(count ==3){ c = seven;tmax3= max3;y =6.5;col3=2;}
    else if(count == 4){c = eight;tmax3= max4;y =80;col3=3;}
    scale(tmax1,tmax2,tmax3,tmax4,w,x,y,z,col1,col2,col3,col4); 
  }
  //4th axes
  else if(selector4 ==4){
    if(count ==1){d= five;tmax4= max1;z =1/2.5;col4=0;}
    else if(count ==2){d=six;tmax4= max2;z =1/2.5;col4=1;}
    else if(count ==3){ d = seven;tmax4= max3;z = 6.5;col4=2;}
    else if(count == 4){d = eight;tmax4= max4;z =80;col4=3;}
    scale(tmax1,tmax2,tmax3,tmax4,w,x,y,z,col1,col2,col3,col4); 
  }
  //plotting parallel plot
  line(450,height-a[idx],683,height-b[idx]);
  line(683,height-b[idx],916,height-c[idx]);
  line(916,height-c[idx],1150,height-d[idx]);
  scale(tmax1,tmax2,tmax3,tmax4,w,x,y,z,col1,col2,col3,col4);
  idx ++ ;
  //mouse values
  if((mouseX >= 450 && mouseX <= 683 && mouseY >=  height-a[idx] && (mouseY <= height-b[idx] || (mouseY <= height-a[idx] && mouseY >= height-b[idx])))||
     (mouseX >= 683 && mouseX <=916 && mouseY >= height-b[idx] && (mouseY <= height-c[idx] || (mouseY <= height-b[idx] && mouseY >= height-c[idx])))||
     (mouseX >= 916 && mouseX <=1150 && mouseY >= height-c[idx] &&( mouseY <= height-d[idx] || (mouseY <= height-c[idx] && mouseY >= height-d[idx])))){
        fill(255,0,0);          
       //bar
  text((int)val[idx],10,380-e[idx]);
  line(40,380-e[idx],40.5+idx*1.2,380-e[idx]);
  
 
  //scatterMatrix
  line(50,height-h[idx],50+g[idx],height-h[idx]);
  line(50+g[idx],height-25,50+g[idx],height-h[idx]+2);
  //parallelplot
  stroke(255,0,0);
  strokeWeight(3);
  line(450,height-a[idx],683,height-b[idx]);
  line(683,height-b[idx],916,height-c[idx]);
  line(916,height-c[idx],1150,height-d[idx]);
  strokeWeight(1);
  stroke(0);
  fill(255);
     text("WORKIN",2,2);
   } 
  } 
}