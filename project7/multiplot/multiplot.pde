Table table;
float[] var1, var2, var3, var4, val, val1;
int total,idx=0;
TableRow nw;
float max1,max2,max3,max4;
float[] a, b, c, d, e, f, g, h;
float[] one,two,three,four,hist,wx;

float tmax1=0,tmax2=0,tmax3=0,tmax4=0;
float w,x,y,z;
int col1,col2,col3,col4;

void setup(){
  size(1200, 800);
  selectInput("Select a file to process:", "fileSelected");   
}

public void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  }
  else {
    println("User selected " + selection.getAbsolutePath());
    table = loadTable(selection.getAbsolutePath(),"header,csv");
    total = (int) table.getRowCount();
    var1 = new float[total];
    var2 = new float[total];
    var3 = new float[total];
    var4 = new float[total];
    val = new float[total];
    val1 = new float[total];
    a  =  new float[total];
    b  =  new float[total];
    c  =  new float[total];
    d  =  new float[total]; 
    e = new float[total];
    f = new float[total];
    g = new float[total];
    h = new float[total];
    one =  new float[total];
    two =  new float[total];
    three =  new float[total]; 
    four =  new float[total];
    five =  new float[total];
    six =  new float[total];
    seven =  new float[total];
    eight =  new float[total];
    hist =  new float[total];
    wx = new float[total];
    hist[0]=0; hist[1] =0;hist[2]=0;hist[3] =0;
    for(int i=0; i < total ; i++){
      nw = table.getRow(i);
      var1[i] = nw.getFloat(0); 
      var2[i] = nw.getFloat(1);
      var3[i] = nw.getFloat(2);
      var4[i] = nw.getFloat(3);  
    }
    
    max1 = max(var1);
    max2 = max(var2);
    max3 = max(var3);
    max4 = max(var4);
  } 
}


void  draw(){    
 
  strokeWeight(3);
  fill(255, 255, 157);
  //bar
  rect(0,0,width/3,height/2);
  //line
  rect(width/3,0,width/3,height/2);
  //scatterplot
  rect(2*width/3,0,width/3,height/2);
  //scatterplot matrix
  rect(0,height/2,width/3,height/2);
 
  //parallel plot
  rect(height/2,width/3,2*width/3,height/2);
  strokeWeight(1);
  histogram();
  scatterMatrix();
  bar();
  //linechart();
  corrgram();
  
  
  parallelPlot();
  
 // highlight(idx);
 

}